import cmd, shlex, getpass, socket, sys, ConfigParser, os
from threading import Thread

s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
C = None
default_url = "https://www.box.net/dav/"
default_user = "random@courseguide.info"
default_pass = "Uv6$3UnNF4r3znveWx*YDr87CRxfnzp#"

def parse_config():
    config = ConfigParser.RawConfigParser()
    print os.getcwd()
    try:
        config.read('config.ini')
    except:
        print "Error: no config.ini file present, no folders will sync. (1)"
    #print config.get('User', 'Name')
    try:
        folder = config.get('Paths', 'Root')
    except:
        print "Error: Root not specified in config.ini, no folders will sync. (2)"
        folder = '/home/eric/libmew/meweb/files/'
    return folder

def send_data(cmds, data):
    print "Sending data to dameon"
    if s is None:
        print "No connection to socket, no data being sent."
        return
    out = cmds + " " + data + " \r\n\r\n"
    s.send(out)
    size = 1024
    retmsg = s.recv(size)

def get_folder():
    print "getting folder from dameon"
    if s is None:
        print "No connection to socket, no data being sent."
        return None
    size = 1024
    s.send("get_dir \r\n\r\n")
    retval = s.recv(size)
    return retval

def run_get_put(line, cmds):
    args = shlex.split(line)
    #print args
    if len(args) == 0:
        print "No files specified."
        return

    for path in args:
        print cmds.capitalize() + ": " + path
        send_data(cmds, path)
    

def get(line):
    print "getting"
    run_get_put(line, "get")
    #C.get_file(file_name = path, obj= 'outfile')
    print "Get complete."

def put(line):
    print "putting"
    run_get_put(line, "put")
    #C.put_file(file_name = line, obj = 'outfile')
    print "Put complete."
    
def sync():
    send_data("sync", "")
    print 'Sync complete.x'

#change to do_connect if this is fixed and ready to hook back up
def connect(line):
    args = shlex.split(line)
    if len(args) > 2:
        print 'Incorrect syntax, use: connect <url> <username>'
        return
    nurl = None
    nuser = None
    if len(args) > 0:
        nurl = args[0]
    if len(args) > 1:
        nuser = args[1]
    
    passd = default_user
    if nuser != None:
        passd = getpass.getpass() #need to prompt user for pass

    if nuser == None and nurl == None:
        print 'no url or user'
        #C.connect(user=default_user, url = default_url, password = passd)
    if nuser == None and nurl != None:
        print 'no user'
        #C.connect(user=default_user, url = nurl, password = passd)
    if nuser != None and nurl != None:
        print 'user and url specified'
        #C.connect(user=nuser, url = nurl, password = passd)
    print 'Connected'

def open_socket():
    global C
    global s
    #connector.start('webdav')
    #C = connector.Connector('webdav')
    host = 'localhost'
    port = 8888
    #try and connect to port
    try: 
        print "connecting to socket"
        s.connect((host,port)) 
    except socket.error, (value,message):
        if s: 
            s.close() 
        print "Could not open socket: " + message 
        return "Failed."
    print "done opening socket: " + bin(s.fileno())

def close_socket():
    print "closing"
    s.close()
