from django.conf.urls.defaults import patterns, include, url
from meweb.client.views import *
from django.shortcuts import render_to_response
from meweb.views import *


# Uncomment the next two lines to enable the admin:
# from django.contrib import admin
# admin.autodiscover()

urlpatterns = patterns('',
	url (r'^about/$', display_static, {'template': 'about.html'}),
	url (r'^help/$', display_static, {'template': 'help.html'}),
    url (r'^$', list_files),
    url (r'^sync/$', sync),
    url (r'^pull_all/$', pull_all),
    url (r'^push_all/$', push_all),
    url (r'^push_file/$', push_file),
    url (r'^pull_file/$', pull_file),
    url (r'^delete_file/$', delete_file),

    # Examples:
    # url(r'^$', 'meweb.views.home', name='home'),
    # url(r'^meweb/', include('meweb.foo.urls')),

    # Uncomment the admin/doc line below to enable admin documentation:
    # url(r'^admin/doc/', include('django.contrib.admindocs.urls')),

    # Uncomment the next line to enable the admin:
    # url(r'^admin/', include(admin.site.urls)),
)
