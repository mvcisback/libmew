from django.shortcuts import render_to_response
from django.template import RequestContext

def display_static(request,template='error.html'):
    c = {'user':request.user}
    return render_to_response(template, c, context_instance=RequestContext(request))
