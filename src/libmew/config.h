/* Configuration Parser for all libmew utilities
   Please use this for any utility you make */

#ifndef _CONFIG_H_
#define _CONFIG_H_

typedef struct _config_t config_t;

int config_init (config_t ** c);

void config_destroy (config_t * c);

#endif
